package com.gz.elasticsearch;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.CreateResponse;
import co.elastic.clients.elasticsearch.core.GetResponse;
import co.elastic.clients.elasticsearch.indices.CreateIndexResponse;
import co.elastic.clients.transport.endpoints.BooleanResponse;
import com.gz.elasticsearch.entity.TestDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@SpringBootTest
@RunWith(SpringRunner.class)
class EsApplicationTests {
    @Autowired
    private ElasticsearchClient client;

    /**
     * 创建索引
     *
     * @throws IOException
     */
    @Test
    public void createIndex() throws IOException {

        CreateIndexResponse products = client.indices().create(c -> c.index("db_idx5"));
        System.out.println(products.acknowledged());

    }

    /**
     * 判断索引是否存在
     *
     * @throws IOException
     */
    @Test
    public void createExi() throws IOException {
        BooleanResponse exists = client.indices().exists(e -> e.index("db_idx5"));
        System.out.println(exists.value());

    }

}

