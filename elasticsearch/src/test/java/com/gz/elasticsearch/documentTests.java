package com.gz.elasticsearch;

import cn.hutool.json.JSONUtil;

import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.IndexResponse;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gz.elasticsearch.entity.TestDTO;
import com.gz.elasticsearch.service.DocumentDemoService;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
class documentTests {

    private final static String INDEX_NAME = "db_api_idx_uservo";

    @Autowired
    private DocumentDemoService documentDemoService;


    @Test
    public void testCreateByFluentDSL() throws Exception {
        // 构建文档数据
        TestDTO userVO = new TestDTO();
        userVO.setId(1L);

        // 新增一个文档
        IndexResponse response = documentDemoService.createByFluentDSL(INDEX_NAME, userVO.getId().toString(), userVO);

        System.out.println("response.forcedRefresh() -> " + response.forcedRefresh());
        System.out.println("response.toString() -> " + response.toString());
    }

    @Test
    public void testCreateByBuilderPattern() throws Exception {
        // 构建文档数据
        TestDTO userVO = new TestDTO();
        userVO.setId(2L);


        // 新增一个文档
        IndexResponse response = documentDemoService.createByBuilderPattern(INDEX_NAME, userVO.getId().toString(), userVO);

        System.out.println("response.toString() -> " + response.toString());
    }

    @Test
    public void testCreateByJSON() throws Exception {
        // 构建文档数据
        TestDTO userVO = new TestDTO();
        userVO.setId(3L);


        // 新增一个文档
        IndexResponse response = documentDemoService.createByJson(INDEX_NAME, userVO.getId().toString(), JSONUtil.toJsonStr(userVO));

        System.out.println("response.toString() -> " + response.toString());
    }



    @Test
    public void testBulkCreate() throws Exception {


        // 构造文档集合
        List<Object> list = new ArrayList<>();
        for (int i = 5; i <= 7; i++) {
            TestDTO userVO = new TestDTO();
            userVO.setId(Long.valueOf(i));

            list.add(userVO);
        }

        // 批量新增
        BulkResponse response = documentDemoService.bulkCreate(INDEX_NAME, list);
        List<BulkResponseItem> items = response.items();
        for (BulkResponseItem item : items) {
            System.out.println("BulkResponseItem.toString() -> " + item.toString());
        }
    }

    @Test
    public void testGetById() throws Exception {
        Long id = 1L;
        Object object = documentDemoService.getById(INDEX_NAME, id.toString());

        System.out.println("object ->" + object);

    }

    @Test
    public void testGetObjectNode() throws Exception {
        Long id = 1L;
        ObjectNode objectNode = documentDemoService.getObjectNodeById(INDEX_NAME, id.toString());

        Assertions.assertNotNull(objectNode);
        System.out.println("id ->" + objectNode.get("id").asLong());
        System.out.println("userName ->" + objectNode.get("userName").asText());
    }


}

