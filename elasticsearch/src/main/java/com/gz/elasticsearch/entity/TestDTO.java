package com.gz.elasticsearch.entity;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Data
public class TestDTO {
    private Long id;
    private String brand;
    private String category;
    private String images;
    private Double price;
    private String title;


}
