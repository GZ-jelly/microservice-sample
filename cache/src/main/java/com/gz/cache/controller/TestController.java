package com.gz.cache.controller;

import com.github.benmanes.caffeine.cache.Cache;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class TestController {
    @Resource
    private Cache<String,Object> caffeineCache;
    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @GetMapping("/test-caffeine")
    public String testCaffeine(){

        //存数据
        caffeineCache.put("test","hello");
        //读数据
        String result = (String) caffeineCache.asMap().get("test");
        return result;
    }
    @GetMapping("/test-redis")
    public String testRedis(){
        redisTemplate.opsForValue().set("name","沉默王二");
        return (String) redisTemplate.opsForValue().get("name");
    }
}
