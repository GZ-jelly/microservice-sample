package com.gz.rate.limit.controller;

import com.gz.rate.limit.guava.RateConfigAnno;
import com.gz.rate.limit.sentinel.SentinelLimitAnnotation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @GetMapping("/test-guava")
    @RateConfigAnno(limitType = "test",limitCount = 1)
    public String test() {
        return "guava";
    }
    @GetMapping("/test-sentinel")
    @SentinelLimitAnnotation(limitCount = 1,resourceName = "sentinelLimit")
    public String sentinelLimit(){
        return "sentinelLimit";
    }
}
