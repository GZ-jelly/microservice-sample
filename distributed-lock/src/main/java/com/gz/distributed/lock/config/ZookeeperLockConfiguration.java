package com.gz.distributed.lock.config;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.RetryUntilElapsed;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.zookeeper.lock.ZookeeperLockRegistry;

/**
 * zookeeper lock config
 */
@Configuration
public class ZookeeperLockConfiguration {
    /**
     * 注册表
     * @param curatorFramework
     * @return
     */
    @Bean
    public ZookeeperLockRegistry zookeeperLockRegistry(CuratorFramework curatorFramework) {
        return new ZookeeperLockRegistry(curatorFramework, "/locks");
    }

    /**
     * 客户端
     * @return
     * @throws Exception
     */
    @Bean
    public CuratorFramework curatorFramework() throws Exception {
        return CuratorFrameworkFactory.newClient("127.0.0.1:2181", new RetryUntilElapsed(1000, 4));
    }

}

