package com.gz.distributed.lock.service;

public interface LockService {
    void testDistributedLock() throws InterruptedException;

    void testZookeeperLock();
}
