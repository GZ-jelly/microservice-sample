package com.gz.distributed.lock.controller;

import com.gz.distributed.lock.service.LockService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class DistributedController {

    private final LockService lockService;

    @GetMapping("/testRedis")
    public String testRedis() throws InterruptedException {

        lockService.testDistributedLock();
        return "that is ok";
    }
}
