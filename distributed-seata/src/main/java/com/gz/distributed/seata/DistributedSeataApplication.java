package com.gz.distributed.seata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DistributedSeataApplication {

    public static void main(String[] args) {
        SpringApplication.run(DistributedSeataApplication.class, args);
    }

}
