package com.gz.distributed.seata.service.impl;

import com.gz.distributed.seata.mapper.master.OrderMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderMapper orderMapper;
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update() {

        return orderMapper.update();
    }
}
