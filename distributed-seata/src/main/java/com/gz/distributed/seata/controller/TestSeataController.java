package com.gz.distributed.seata.controller;

import com.gz.distributed.seata.service.impl.AccountService;
import com.gz.distributed.seata.service.impl.OrderService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class TestSeataController {

    private final OrderService orderService;

    private final AccountService accountService;
    @GetMapping(value = "test-tx")
    @GlobalTransactional
    public Object testCommit() {
        //账号服务，抛异常
        int count = accountService.count();
        //订单服务更新订单
        int updateResult=orderService.update();

        return "ok";

    }
}
