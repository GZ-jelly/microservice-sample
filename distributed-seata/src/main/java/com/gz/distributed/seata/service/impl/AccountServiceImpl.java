package com.gz.distributed.seata.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccountServiceImpl implements AccountService{
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int count() {

        //模拟抛出异常
       /* int numOne=0;
        int numTwo=10;*/
        return 1;
    }
}
