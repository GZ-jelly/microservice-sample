package com.gz.sharding.jdbc.controller;

import com.gz.sharding.jdbc.entity.Order;
import com.gz.sharding.jdbc.mapper.OrderMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ControllerJdbc {
    private final OrderMapper orderMapper;
    @GetMapping(value = "test-jdbc")
    public String testJdbc()  {
        for(int i=1;i<11;i++){
            Order order=new Order();
            order.setNum(i);
            orderMapper.insertUser(order);
        }
        return "ok";
    }

}
