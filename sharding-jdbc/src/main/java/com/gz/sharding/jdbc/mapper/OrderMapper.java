package com.gz.sharding.jdbc.mapper;

import com.gz.sharding.jdbc.entity.Order;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderMapper {
    int insertUser(Order order);
}
