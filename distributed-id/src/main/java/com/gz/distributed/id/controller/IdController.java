package com.gz.distributed.id.controller;

import com.baidu.fsg.uid.UidGenerator;
import com.sankuai.inf.leaf.service.SnowflakeService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class IdController {

    @Resource
    private UidGenerator uidGenerator;
    @Resource
    private SnowflakeService snowflakeService;

    @GetMapping("/get-uuid")
    public String getUid() {
        return String.valueOf(uidGenerator.getUID());
    }
    @GetMapping("/get-leaf")
    public String getLeaf() {
        return String.valueOf(snowflakeService.getId("id"));
    }

}
