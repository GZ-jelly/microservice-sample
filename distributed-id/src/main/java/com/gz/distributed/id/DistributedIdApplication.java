package com.gz.distributed.id;

import com.sankuai.inf.leaf.plugin.annotation.EnableLeafServer;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(basePackages = {"com.gz.distributed.id","com.baidu.fsg","com.sankuai.inf.leaf"})
@MapperScan("com.gz.distributed.id.mapper")
@EnableLeafServer
@SpringBootApplication
public class DistributedIdApplication {

    public static void main(String[] args) {
        SpringApplication.run(DistributedIdApplication.class, args);
    }

}
