package com.gz.distributed.id.config;

import com.baidu.fsg.uid.impl.CachedUidGenerator;
import com.gz.distributed.id.assigner.CustomDisposableWorkerIdAssigner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IdGeneratorConfiguration {


    /*
        该类是在默认的基础上修改的
    */
    @Bean
    public CustomDisposableWorkerIdAssigner disposableWorkerIdAssigner() {
        return new CustomDisposableWorkerIdAssigner();
    }
    //默认注入的id生成器,使用时只需从容器取即可
    @Bean
    public CachedUidGenerator cachedUidGenerator() {
        CachedUidGenerator cachedUidGenerator = new CachedUidGenerator();
        cachedUidGenerator.setWorkerIdAssigner(disposableWorkerIdAssigner());
        return cachedUidGenerator;
    }


}

